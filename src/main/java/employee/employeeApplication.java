package employee;

import employee.dao.PersonDAO;
import employee.dao.TodoDAO;
import employee.models.Person;
import employee.resources.PersonResource;
import employee.resources.TodoResource;
import employee.service.PersonService;
import employee.service.TodoService;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.jdbi3.JdbiFactory;
import org.jdbi.v3.core.Jdbi;

import java.util.List;

public class employeeApplication extends Application<employeeConfiguration> {

    public static void main(final String[] args) throws Exception {
        new employeeApplication().run(args);
    }

    @Override
    public String getName() {
        return "employee";
    }

    @Override
    public void initialize(final Bootstrap<employeeConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final employeeConfiguration configuration,
                    final Environment environment) {
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, configuration.getDataSourceFactory(), "mysql");
//        For testing DB connection
//        List<String> names = jdbi.withHandle(handle ->
//                handle.createQuery("select name from persons")
//                        .mapTo(String.class)
//                        .list());
//        System.out.println(names);
//        environment.jersey().register(new (jdbi));
        final PersonDAO personDAO = jdbi.onDemand(PersonDAO.class);
        final PersonResource personResource = new PersonResource(new PersonService(personDAO));
        environment.jersey().register(personResource);
//        Listing todo resource endpoints
        final TodoDAO todoDAO = jdbi.onDemand(TodoDAO.class);
        final TodoResource todoResource = new TodoResource(new TodoService(todoDAO));
        environment.jersey().register(todoResource);
    }

}
