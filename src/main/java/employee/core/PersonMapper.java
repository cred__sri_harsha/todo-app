package employee.core;


import employee.models.Person;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonMapper implements RowMapper<Person> {
    public PersonMapper() {
    }

    @Override
    public Person map(ResultSet resultSet, StatementContext statementContext) throws SQLException {
        Person person = new Person();
        person.setId(resultSet.getString("id"));
        person.setName(resultSet.getString("name"));
        person.setEmail(resultSet.getString("email"));
        person.setAge(resultSet.getInt("age"));
        return person;
    }
}
