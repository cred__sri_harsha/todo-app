package employee.core;

import employee.models.Todo;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TodoMapper implements RowMapper<Todo> {
    public TodoMapper() {
    }

    @Override
    public Todo map(ResultSet resultSet, StatementContext statementContext) throws SQLException {
        Todo todo = new Todo();
        todo.setId(resultSet.getString("id"));
        todo.setDone(resultSet.getBoolean("done"));
        todo.setPerson(resultSet.getString("person"));
        todo.setToDo(resultSet.getString("todo"));
        return todo;
    }
}
