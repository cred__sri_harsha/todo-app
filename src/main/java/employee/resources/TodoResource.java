package employee.resources;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.sun.research.ws.wadl.HTTPMethods;
import employee.models.Todo;
import employee.service.TodoService;
import jdk.nashorn.api.scripting.JSObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URISyntaxException;
import java.util.List;

@Path("/api/todo")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class TodoResource {
    private final TodoService todoService;

    public TodoResource(TodoService todoService) {
        this.todoService = todoService;
    }

    @GET
    @Path("{todo}")
    public Todo getTodo(@PathParam("todo") String todo) {
        return todoService.getTodo(todo);
    }

    @GET
    public List<Todo> getPersonTodos(@QueryParam("person") String person) {
        return todoService.getPersonTodos(person);
    }

    @POST
    public Response createTodo(Todo todo) throws URISyntaxException {
        todoService.createTodo(todo);
        return Response.status(Response.Status.OK).entity("{}").build();
    }

    @PUT
    public Response updateTodo(Todo todo) throws URISyntaxException {
        todoService.updateTodo(todo);
        return Response.status(Response.Status.OK).entity("{}").build();
    }
}
