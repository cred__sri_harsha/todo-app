package employee.resources;


import employee.models.Person;
import employee.service.PersonService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.net.URISyntaxException;
import java.util.List;

@Path("/api/person")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class PersonResource {
    private final PersonService personService;

    public PersonResource(PersonService personService) {
        this.personService = personService;
    }

    @GET
    public List<Person> getPerson() {
        return personService.getAllPeople();
    }

    @GET
    @Path("{id}")
    public Person getPerson(@PathParam("id") String id) {
        return personService.getById(id);
    }

    @POST
    public int createPerson(Person person) throws URISyntaxException {
        personService.createPerson(person);
        return 1;
    }

}
