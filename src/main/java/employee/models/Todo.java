package employee.models;

public class Todo {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String person;
    private String toDo;
    private boolean isDone;

    public Todo() {
    }

    public Todo(String person, String toDo, boolean isDone) {
        this.person = person;
        this.toDo = toDo;
        this.isDone = isDone;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getToDo() {
        return toDo;
    }

    public void setToDo(String toDo) {
        this.toDo = toDo;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    @Override
    public String toString() {
        return "Todo{" +
                "person=" + person +
                ", toDo='" + toDo + '\'' +
                ", isDone=" + isDone +
                '}';
    }
}
