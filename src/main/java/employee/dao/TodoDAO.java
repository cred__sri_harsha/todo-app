package employee.dao;

import employee.core.TodoMapper;
import employee.models.Person;
import employee.models.Todo;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

@RegisterRowMapper(TodoMapper.class)
public interface TodoDAO {

    @SqlUpdate("INSERT INTO todo(person, todo, done) VALUES(:t.person, :t.toDo, :t.done)")
    void createTodo(@BindBean ("t") Todo todo);

    @SqlUpdate("UPDATE todo SET person = :t.person, todo = :t.toDo, done = :t.done WHERE id = :t.id")
    void updateTodo(@BindBean ("t") Todo todo);

    @SqlQuery("SELECT * FROM todo WHERE id = :id")
    Todo getTodobyId(@Bind ("id") String id);

    @SqlQuery("SELECT * FROM todo WHERE person = :person")
    List<Todo> getPersonTodos(@Bind ("person") String person);
}
