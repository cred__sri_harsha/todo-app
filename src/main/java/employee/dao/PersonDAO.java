package employee.dao;

import employee.core.PersonMapper;
import employee.models.Person;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

@RegisterRowMapper(PersonMapper.class)
public interface PersonDAO {

    @SqlUpdate("INSERT INTO persons(id, name, age, email) VALUES (:p.id, :p.name, :p.age, :p.email)")
    void createPerson(@BindBean ("p") Person person);

    @SqlQuery("select * from persons")
    List<Person> getAllPeople();

    @SqlQuery("select * from persons where id = :id")
    Person getById(@Bind ("id") String id);
}
