package employee.service;

import employee.dao.PersonDAO;
import employee.models.Person;

import java.util.ArrayList;
import java.util.List;

public class PersonService {
    private PersonDAO personDAO;

    public PersonService(PersonDAO personDAO) {
        this.personDAO = personDAO;
    }

    public List<Person> getAllPeople() {
        return personDAO.getAllPeople();
    }

    public Person getById(String id) {
        return personDAO.getById(id);
    }

    public void createPerson(Person person) {
//        System.out.println(person.toString());
        person.setId();
        personDAO.createPerson(person);
    }
}
