package employee.service;

import employee.dao.TodoDAO;
import employee.models.Todo;

import java.util.List;

public class TodoService {
    private TodoDAO todoDAO;

    public TodoService(TodoDAO todoDAO) {
        this.todoDAO = todoDAO;
    }

    public Todo getTodo(String todo) {
        return todoDAO.getTodobyId(todo);
    }

    public List<Todo> getPersonTodos(String person) {
        return todoDAO.getPersonTodos(person);
    }

    public void createTodo(Todo todo) {
        todoDAO.createTodo(todo);
    }

    public void updateTodo(Todo todo) {
        todoDAO.updateTodo(todo);
    }
}
