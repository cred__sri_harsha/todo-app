# TODO


Configuration
---

Change the DB settings in config.yml file to match your db:pass and Database.

- Create tables `persons` and `todo` in the DB.

```
create table persons(id INTEGER PRIMARY KEY, name VARCHAR(255), age INT, email VARCHAR(255));
```

```
create table todo(id int AUTO_INCREMENT, todo TEXT(1000), person VARCHAR(255) REFERENCES persons(id), done BOOLEAN);
```

These 2 tables will be used to populate user details and todo details respectively.

How to start the employee application
---

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/employee-1.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`

Health Check
---

To see your applications health enter url `http://localhost:8081/healthcheck`


Sample cURL Requests
---

Creating a user
```
curl -X POST \
  http://localhost:8080/api/person \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 7f0d4921-4b2b-153c-d03a-94c0d213c650' \
  -d '{
	"name": "goku",
	"email": "goku@test.com",
	"age": 14
}'
```
GET all users
```
curl -X GET \
  http://localhost:8080/api/person/ \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 030a41cd-9b46-0b44-0fe7-8d26fbd05687'
```
GET one user
```
curl -X GET \
  http://localhost:8080/api/person/0440ac68-be07-44c2-b9b5-809300fa6799 \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 243a7821-bfef-1a72-b5ee-b6f44464f3a6'
```

GET Todos of a user
```
curl -X GET \
  'http://localhost:8080/api/todo/?person=00bc1875-69e8-47ae-8087-deee076702d0' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 73e91844-e399-353c-96a9-1cd1ed509c8b'
```

POST - Create a TODO for a User
```
curl -X POST \
  http://localhost:8080/api/todo/ \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: cc004175-6d90-0c48-d237-02f3b5658643' \
  -d '{
	"person": "0440ac68-be07-44c2-b9b5-809300fa6799",
	"toDo": "Test todo",
	"done": false
}'
```
PUT - Updating a TODO
```
curl -X PUT \
  http://localhost:8080/api/todo/ \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: b8771791-f42f-e622-dca0-bafbf40d26c0' \
  -d '{
	"id": 14,
	"person": "0440ac68-be07-44c2-b9b5-809300fa6799",
	"toDo": "Test todo",
	"done": true
}'
```

Code Flow
---

1. Create a model that is required.
2. Create a Mapper corresponding to the model.
3. Create a DAO interface using the mappers for a model.
4. Create a Service for getting response using DAO.
5. Write a Resource/API using the service.
6. Adding rules for API in Application.
